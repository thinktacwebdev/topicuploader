import * as functions from 'firebase-functions';
const admin = require('firebase-admin')
const serviceAccount = require('./google-services')
const cors = require('cors')({ origin: true });

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://testapp-bb252.firebaseio.com"
});
const db = admin.firestore();
//firebase deploy --only functions:topicUpload

export const topicUpload = functions.https.onRequest(async (request, response) => {
  cors(request, response, async () => {

    if (request.method === "POST") {
      const body = request.body;
      const csvObject = body.csvData;
      const board = body.board
      await updateTopicColl(csvObject, board);
      response.status(200).send(JSON.stringify("ok"));

    } else {
      // console.error('Request method not allowed.');
      response.status(405).send(JSON.stringify('Method Not Allowed'));
    }
  });
});

async function updateTopicColl(csvData: any, boardSelected: any) {
  const topicName = boardSelected + " Topics"

  await db.collection('Topic').doc(boardSelected).get()
    .then(async (doc: any) => {
      if (!doc.exists || !doc.data()[topicName]) {

        /* Write into board selected document under Topic Collection */
        await db.collection('Topic').doc(boardSelected).set({
          [topicName]: admin.firestore.FieldValue.arrayUnion(...csvData)
        }, { merge: true })
      } else {

        /* Update into board selected document under Topic Collection */
        const topicDoc = await doc.data()[topicName];
        const topicDocUpdate = await doc.data()[topicName]
        for (const topic of csvData) {

          const index = await topicDoc.findIndex((element: any) => element.TAG === topic.TAG)
          if (index >= 0) {
            topicDocUpdate[index] = topic;
          } else {
            topicDocUpdate.push(topic)
          }
        }

        await db.collection('Topic').doc(boardSelected).set({
          [topicName]: topicDocUpdate
        }, { merge: true })
      }
    })
  await processCsvObj(csvData, boardSelected)
}

async function processCsvObj(csvData: any, boardSelected: any) {
  const TAGMaster: any = []
  const TAGToTACAsc: any = []
  let TACToTAGAsc: any = []

  for (const csvRowObj of Object.values(csvData)) {
    const value: any = csvRowObj;

    /* TACMaster Document data structure */
    TAGMaster.push({
      'TAG Group': 'Topics',
      'TAG': value['TAG'] ? value['TAG'] : '',
      'Attribute': value['Topic Name'] ? value['Topic Name'] : '',
      'TAG Category': boardSelected,
      'Description': value['Leaf Topic'] ? value['Leaf Topic'] : '',
    })

    /* TAGToTACAssociation Document data structure -> TAG:[tacid1,tacid2, ...]*/
    if (value['TAG']) {
      const tacs = [value['TAC 1'], value['TAC 2'], value['TAC 3'], value['TAC 4'], value['TAC 5']];
      const definedTacs = tacs.filter(function (element) {
        return element !== undefined;
      });
      TAGToTACAsc.push(
        {
          tagId: value['TAG'],
          tacValue: definedTacs
        }
      )
    }

    /* Tac collection data structure  -> tacId document can have multiple TAG*/
    if (value['TAC 1']) {
      TACToTAGAsc = await TACtoTAGMap(value['TAC 1'], TACToTAGAsc, boardSelected, value)
    }
    if (value['TAC 2']) {
      TACToTAGAsc = await TACtoTAGMap(value['TAC 2'], TACToTAGAsc, boardSelected, value)
    }
    if (value['TAC 3']) {
      TACToTAGAsc = await TACtoTAGMap(value['TAC 3'], TACToTAGAsc, boardSelected, value)
    }
    if (value['TAC 4']) {
      TACToTAGAsc = await TACtoTAGMap(value['TAC 4'], TACToTAGAsc, boardSelected, value)

    }
    if (value['TAC 5']) {
      TACToTAGAsc = await TACtoTAGMap(value['TAC 5'], TACToTAGAsc, boardSelected, value)
    }
  }

  await updateTAGMaster(TAGMaster, boardSelected)
  await updateTAGTOTACASC(TAGToTACAsc, boardSelected)
  await updateTacColl(TACToTAGAsc);
}

async function TACtoTAGMap(tacId: any, TACToTAGAsc: any, boardSelected: any, value: any) {
  const index = await TACToTAGAsc.findIndex((element: any) => element.tacId === tacId)
  const tagObj = {
    'TAG Group': 'Topics',
    'TAG': value['TAG'] ? value['TAG'] : '',
    'Attribute': value['Topic Name'] ? value['Topic Name'] : '',
    'TAG Category': boardSelected,
    'Description': value['Leaf Topic'] ? value['Leaf Topic'] : '',
  }
  if (index >= 0) {
    TACToTAGAsc[index].tagValue.push(tagObj)
  } else {
    TACToTAGAsc.push({
      tacId: tacId,
      tagValue: [tagObj]
    })
  }
  return TACToTAGAsc
}
async function updateTAGMaster(TAGMaster: any, boardSelected: any) {
  const topicName = boardSelected;

  await db.collection("Tags").doc("TAGMaster").get()
    .then(async (doc: any) => {
      if (!doc.exists || !doc.data()[topicName]) {

        /* Write into TAGMaster document under Tags Collection */
        await db.collection("Tags").doc("TAGMaster").set({
          [topicName]: admin.firestore.FieldValue.arrayUnion(...TAGMaster)
        }, { merge: true })
      } else {

        /* Update into TAGMaster document under Tags Collection */
        const tagDoc = await doc.data()[topicName];
        const tagDocUpdate = await doc.data()[topicName]
        for (const tags of TAGMaster) {

          const index = await tagDoc.findIndex((element: any) => element.TAG === tags.TAG)
          if (index >= 0) {
            tagDocUpdate[index] = tags;
          } else {
            tagDocUpdate.push(tags)
          }
        }

        await db.collection("Tags").doc("TAGMaster").set({
          [topicName]: tagDocUpdate
        }, { merge: true })
      }
    })
}

async function updateTAGTOTACASC(TAGToTACAsc: any, boardSelected: any) {
  for (const tag of TAGToTACAsc) {
    if (tag.tacValue.length > 0) {
      await db.collection("Tags").doc("TAGToTACAssociation").set({
        [tag.tagId]: admin.firestore.FieldValue.arrayUnion(...tag.tacValue)
      }, { merge: true })
    }
  }
}

async function updateTacColl(TAC: any) {
  for (const tac of TAC) {
    if (tac.tagValue.length) {

      await db.collection('Tac').doc(tac.tacId).get()
        .then(async (doc: any) => {
          if (!(doc.exists) || !(doc.data().tags)) {

            /* Write into tacId document under Tac Collection */
            await db.collection("Tac").doc(tac.tacId).set({
              'tags': admin.firestore.FieldValue.arrayUnion(...tac.tagValue),
              'TAC_Code': tac.tacId
            }, { merge: true })
          } else {

            /* Update into tacId document under Tac Collection */
            const tacDoc = await doc.data().tags;
            const tacDocUpdate = await doc.data().tags;
            for (const tagMap of tac.tagValue) {

              const index = await tacDoc.findIndex((element: any) => element.TAG === tagMap.TAG)
              if (index >= 0) {
                tacDocUpdate[index] = tagMap;
              } else {
                tacDocUpdate.push(tagMap)
              }
            }
            await db.collection("Tac").doc(tac.tacId).set({
              'tags': tacDocUpdate,
              'TAC_Code': tac.tacId
            }, { merge: true })
          }
        })
    }
  }
}